import { VoterBookPage } from './app.po';

describe('voter-book App', () => {
  let page: VoterBookPage;

  beforeEach(() => {
    page = new VoterBookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
