import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "app/auth.service";
import { LocalDataService } from "app/LocalDataService";

@Component({
  selector: 'vb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  id: number;
  showerror: boolean = false;
  errorMessage: string = "";
  constructor(private localdata: LocalDataService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
  onLogin(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;
    if (form.value.email.length <= 0) {
      this.errorMessage = "Please Enter username";
      this.showerror = true;
      return;
    }
    if (form.value.password.length <= 0) {
      this.errorMessage = "Please Enter password";
      this.showerror = true;
      return;
    }


    this.authService.Authenticate(email, password)
      .then(res => {
        this.ValidateLoginUser(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  //Validating the Login Creattiansl
  ValidateLoginUser(userdetails: any) {
    console.log(userdetails[0]);
    if (userdetails[0] === undefined) {
      this.errorMessage = "username or Password invalid"
      this.showerror = true;
      return console.log('');
    }

    else if (userdetails[0].UserTypeName === "Admin") {
      this.localdata.setUserDetails(userdetails[0]);
       this.router.navigate(['/admin']);
    }

  }

}
