import { Http, Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/topromise';
import { Injectable } from "@angular/core";
import { Users } from "app/admin/users/users.model";
import { Router } from "@angular/router";
import { Survey } from "app/admin/survey/survey.model";

@Injectable()
export class VoteBookService {
    private survey: Survey[] = [];
    private users: Users[] = [];
  private URL: string = "http://desktop-mm3gtt0/TestAPI/api/"
   // private URL:string='http://localhost:2022/api/';
    // private URL:string='http://67.225.171.204/plesk-site-preview/api.voterbook.net/67.225.171.204/api/';
    constructor(private http: Http, private router: Router) {

    }
    GetUsers(): Promise<any> {
        return this.http.get(this.URL + "Users/GetAllUsers").toPromise()
            .then(response => response.json());
    }
    getUserById(index: number) {
        return this.http.get(this.URL + "Users/GetUserbyID/" + index).toPromise()
            .then(response => response.json());
    }

    deleteById(ID) {
        return this.http.delete(this.URL + "Users/DeleteUser/" + ID).toPromise().then(response => response.json());

    }

    GetChartData(surveyid: number) {
        return this.http.get(this.URL + "AdminReports/GetChartData/" + surveyid).toPromise().then(response => response.json());
    }

    GetSurveySummary(surveyid: number) {
        return this.http.get(this.URL + "AdminReports/GetSurveySummary/" + surveyid).toPromise().then(response => response.json());
    }


    AddNewUser(user) {
        console.log(user);
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let body = JSON.stringify(user);
        return this.http.post(this.URL + "Users/AddUser", user, { headers: header })
            .toPromise().then(res => res.json());
    }

    UpdateUser(user, ID) {
        console.log(ID);
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let body = JSON.stringify(user);
        console.log(body);
        console.log(user);
        return this.http.put(this.URL + "Users/UpdateUser/" + ID, user, { headers: header })
            .toPromise().then(res => res.json());
    }
    CheckUsernameinDB(value: string) {
        return this.http.get(this.URL + "Users/CheckUserName/" + value).toPromise().then(res => res.json());
    }
    saveSurvey(survey) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let body = JSON.stringify(survey);
        return this.http.post(this.URL + "SaveSurvey/SaveSurveyName", survey, { headers: header })
            .toPromise().then(res => res.json());
    }
    GetSurveys(): Promise<any> {
        return this.http.get(this.URL + "SaveSurvey/GetAllSurveys").toPromise()
            .then(response => response.json());
    }
    MakeSurveyActive(ID): Promise<any> {
        return this.http.put(this.URL + "SaveSurvey/Active/" + ID, {}).toPromise()
    }
    saveQuestions(questions) {
        console.log(questions);
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let body = JSON.stringify(questions);
        return this.http.post(this.URL + "Questions/SaveQuestions", questions, { headers: header })
            .toPromise().then(res => res.json());
    }
    getQuestions(surveyID: number) {
        return this.http.get(this.URL + "Questions/GetQuestionbySurveyID/" + surveyID).toPromise()
            .then(response => response.json());
    }



    GetSurveySummaryArea(surveyid: number) {
        return this.http.get(this.URL + "AdminReports/GetSurveySummaryArea/" + surveyid).toPromise().then(response => response.json());
    }

    GetSurveySummaryGender(surveyid: number) {
        return this.http.get(this.URL + "AdminReports/GetSurveySummaryGender/" + surveyid).toPromise().then(response => response.json());
    }

    GetSurveySummaryAge(surveyid: number) {
        return this.http.get(this.URL + "AdminReports/GetSurveySummaryAge/" + surveyid).toPromise().then(response => response.json());
    }


}


