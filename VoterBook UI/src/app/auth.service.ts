import { Injectable } from "@angular/core";

import { Router } from "@angular/router";
import { Http,Headers } from "@angular/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/topromise';

@Injectable()
export class AuthService{
    token:string;
    private URL:string ="http://desktop-mm3gtt0/TestAPI/api/"
    // private URL:string='http://localhost:2022/api/';
// private URL:string='http://67.225.171.204/plesk-site-preview/api.voterbook.net/67.225.171.204/api/';
constructor(private router:Router,private http:Http)
{
    
}
Authenticate(username:string, password:string):Promise<any>{
      let header=new Headers();
       header.append('Content-Type','application/json');
        let LoginCredientials = {username:username,password:password}
        let body=JSON.stringify(LoginCredientials);
return  this.http.post(this.URL+"login/Authenticate",body,{headers:header}).toPromise()
  .then(response => response.json()).then(response=>this.token=response);


    
// return this.http.post(this.URL+"SaveSurvey/SaveSurveyName",survey,{headers:header})
// .toPromise().then(res=>res.json());
}
Logout(){
    this.token=null;
}
// authenticate(email:string,password:string){
// firebase.auth().signInWithEmailAndPassword(email,password)
// .then(response=>{
//     this.router.navigate(['/home']);
// }).catch(error=>console.log('Invalid Username or Password'));

// }
isAuthenticated(){
    if(this.token==null){
     this.router.navigate(['/login']);
    }
    return this.token != null;
}
}