import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { Users } from "app/admin/users/users.model";
import { VoteBookService } from "app/vote-book.service";


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UsereditComponent implements OnInit {
  userForm: FormGroup;
  editMode = false;
  id: number;
  users: Users;
  index: number;
  checkbox = false;
  btntext: string;
  howerror: boolean = false;
  Errors:string="";
  UserName:string="";
  constructor(private route: ActivatedRoute, private router: Router, private userService: VoteBookService) { }

  ngOnInit() {
    
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      console.log(this.id);
      if (this.id === 0) {
        this.editMode = false;
        this.btntext = "Add User";
      }
      else {
        this.editMode = true;
        this.btntext = "Save";
        this.userService.getUserById(this.id).then
          (users => {
            this.users = users[0];
            this.UserName=this.users.UserName;
            this.userForm.patchValue(this.users);
            if (this.users.IsActive === 1) {
              this.checkbox = true;
            }
          });
      }
    })
    this.IntilizeForm();
  }

  IntilizeForm() {
    let formControls: {} = {};
    formControls["UserName"] = new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9_-]{3,20}$")]);
    formControls["FirstName"] = new FormControl('', [Validators.required,Validators.pattern("^[A-Za-z]{1,15}$")]);
    formControls["LastName"] = new FormControl('', Validators.pattern("^[A-Za-z]{1,15}$"));
    formControls["EmailId"] = new FormControl('', [Validators.required, Validators.email]);
    formControls["Password"] = new FormControl('', [Validators.required,Validators.pattern("^[A-Za-z0-9_-]{6,15}$")]);
    formControls["MobileNumber"] = new FormControl('', [Validators.pattern("^[0-9]{10}$"),Validators.required]);
    formControls["IsActive"] = new FormControl();
    this.userForm = new FormGroup(formControls);
    
    //this.userForm.valueChanges.subscribe(data=>this.OnValueChanged(data));
  }
  OnValueChanged(data){

  }
  formErrors = {
    'UserName': '',
    'FirstName': '',
    'EmailId':'',
    'Password':'',
    'MobileNumber':''
  };

  validationMessages = {
    'UserName': {
      'required':      'Name is required.',
      'minlength':     'Name must be at least 4 characters long.',
      'maxlength':     'Name cannot be more than 24 characters long.',
      'pattern':       'Name should not contain spaces or special characters'
    },
    'power': {
      'required': 'Power is required.'
    }
  };
  onSave() {
    if(!this.userForm.valid){
      this.Errors="Please Fill valid User Details"
      return;
    }
    this.Errors="";
    let user = { UserName: '', password: '', firstname: '', lastname: '', mobileno: '', emailid: '', IsActive: '' };
    user.UserName = this.userForm.value.UserName;
    user.password = this.userForm.value.Password;
    user.emailid = this.userForm.value.EmailId;
    user.firstname = this.userForm.value.FirstName;
    user.mobileno = this.userForm.value.MobileNumber;
    user.lastname = this.userForm.value.LastName;
    if (this.userForm.value.IsActive === true) {
      this.userForm.value.IsActive = 1;
      user.IsActive = this.userForm.value.IsActive;
    }
    else {
      this.userForm.value.IsActive = 0;
      user.IsActive = this.userForm.value.IsActive;
    }
    if (this.editMode === true) {
      this.userService.UpdateUser(user, this.id).then(res=>{
        this.router.navigate(['admin/user']);
      });
    }
    else {
      this.userService.AddNewUser(user).then(res=>{
        this.router.navigate(['admin/user']);
      });
    }
    
  }
  onCancel()
  {
    this.router.navigate(['admin/user']);
  }
  Checkusername(value) {
    console.log(value);
    if(this.UserName==value){
      console.log(this.UserName)
       this.howerror = false;
      return;
    }
    if (true) {
      this.userService.CheckUsernameinDB(value).then(x => {
        console.log(x);
        if (x[0].UserName.length > 0) {
          this.howerror = true
        }
        else {
          this.howerror = false;
        }
      }
      )
    }
 

}


}