import { Component, OnInit } from '@angular/core';
import { Params } from "@angular/router/public_api";
import { ActivatedRoute, Router } from "@angular/router";
import { Users } from "app/admin/users/users.model";
import { VoteBookService } from "app/vote-book.service";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  id: number;
  users:Users;
    checkbox = false;

  constructor(private route:ActivatedRoute,private userService:VoteBookService,
  private router:Router) { }

  ngOnInit() {
        this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.userService.getUserById(this.id).then
      (users => {
            this.users = users[0];
            console.log(this.users.IsActive);
            if (this.users.IsActive === 1) {
              this.checkbox = true;
            }
      }
        )}
        );

}
onEdit(users){
  let id = users.UserId;
    this.router.navigate(['../',id,'edit'],{relativeTo:this.route});
}

}