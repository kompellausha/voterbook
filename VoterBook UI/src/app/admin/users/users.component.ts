import { Component, OnInit, Input } from '@angular/core';
import { Http } from "@angular/http";

import { FormGroup, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute, Params } from "@angular/router";

import { Users } from "app/admin/users/users.model";
import { VoteBookService } from "app/vote-book.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Input() users: Users;
  @Input() index: number;
  id: number;
  editMode = false;
  formGroup: FormGroup;
 

  constructor(private http: Http, private usersService: VoteBookService,
    private fb: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.usersService.GetUsers().then(
      res => {
        this.users = res;
        console.log(res);
      }
    )
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      // this.users = this.usersService.getUserById(this.id);
    })
  }
  onView(user) {
    let id = user.UserId;
    this.router.navigate(['./', id], { relativeTo: this.route });
  }
  onEdit(user) {
    user.Edit = true;
    let id = user.UserId;
    this.router.navigate(['./', id, 'edit'], { relativeTo: this.route });
  }
  onAddUser() {
    this.router.navigate(['./', 0, 'new'], { relativeTo: this.route });
  }
  onDelete(user) {
    // let id=user.UserId;
    this.usersService.deleteById(user.UserId).then(res=>{
      this.router.navigate(['admin/user']);;
    });
  }
  OnSave(user) {
    user.Edit = false;
  }
}
