import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { VoteBookService } from "app/vote-book.service";
import { ElementRef, Renderer2, ViewChild, OnChanges, SimpleChanges } from '@angular/core';






@Component({
  selector: 'app-ChartReportDetail',
  templateUrl: './ChartReports.component.html',
  styleUrls: ['./ChartReports.component.css']
})
export class ChartReportsComponent implements OnInit, OnChanges {
  ngOnChanges(changes: SimpleChanges): void {
    console.log("tests", changes);
  }

  options: object[] = [];
  ChartData: any[] = [];
  ChartType: string = "pie";
  doSetData: boolean = false;
  SurveyName: string = '';
  id: number;

  constructor(private router: Router, private route: ActivatedRoute, private voterbookservice: VoteBookService) {

  }



  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.voterbookservice.GetChartData(this.id).then(x => {
        console.log(x);
      this.ChartData = x
      this.GetObjectbyChartType();
    });
    })


  }


  Changechart(value, index: number) {
    this.GetObjectbyChartType(index, value);
  }


  GetObjectbyChartType(ind = 0, type = "pie") {
    this.options = [];
    if (this.ChartData.length > 0) {
      let index = 0;
      this.ChartData.forEach(element => {
        this.SurveyName = element.SurveyName;
        let Options = element.OptionsCount
        let optionsDesc = element.OptionDesc;
        var Datafinal = {};
        optionsDesc.forEach((key, i) => {
          Datafinal[key] = Options[i]
        });
        let Data: any[] = [];
        let localchart = this.ChartType;
        if (index === ind) {
          localchart = type;
        }
        if (localchart.toString() === "pie") {
          for (const key of Object.keys(Datafinal)) {
            let dataform = { name: '', y: 0 };
            dataform.name = key;
            dataform.y = Datafinal[key];
            Data.push(dataform);
          }
               
          let option =
            {
              chart: {
                type: localchart
              },

              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: true,
                    format: '<label><b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)</label>',
                    // <b>{point.name}</b>:
                    //     distance:-20,
                  },
                  showInLegend: true
                }
              },
              title: { text: '' },
              tooltip: {
                //  pointFormat: '{series.name}: <b>{point.y}</b>'
                pointFormat: '{series.name}: <b><b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)</b>'
              },
              xAxis: {
                categories: element.OptionDesc
              },
              series: [{
                name: 'selected:',
                colorByPoint: true,
                data: Data
              }],
              raw: [
                {
                  Question: element.QuestionDesc,
                  QuestionID: element.QuestionID,
                  Chartt: "pie",
                  Answered: element.Answered,
                  skipped: element.Skipped
                }
              ]
            }
          this.options.push(option);
        }

        if (localchart === "bar") {

          let option =
            {
              chart: {
                type: localchart
              },
              plotOptions: {
                bar: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: true
                  }
                }
              },
              title: { text: '' },
              tooltip: {
                valuesuffix: 'selected'
              },
              xAxis: {
                categories: element.OptionDesc,
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'selected ',
                  align: 'high'
                },
                labels: {
                  overflow: 'justify'
                }

              },
              series: [{
                showInLegend: false,
                name: 'selected',
                data: element.OptionsCount
              }],
              raw: [
                {
                  Question: element.QuestionDesc,
                  QuestionID: element.QuestionID,
                  Chartt: "pie",
                  Answered: element.Answered,
                  skipped: element.Skipped
                }
              ]

            }
          this.options.push(option);

        }

        if (localchart === "column") {
          let option =
            {
              chart: {
                type: localchart
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              title: { text: '' },
              tooltip: {
                pointFormat: '{series.name}: <b><b>{point.y}</b> (<b>{point.percentage:.0f}%</b>)</b>'
                // headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                // '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                // footerFormat: '</table>',
                // shared: true,
                // useHTML: true
              },
              xAxis: {
                categories: element.OptionDesc
              },
              yAxis: {
                min: 0,
                title: {
                  text: '',
                },

              },
              series: [{
                showInLegend: false,
                name: 'selected',
                data: element.OptionsCount
              }],
              raw: [
                {
                  Question: element.QuestionDesc,
                  QuestionID: element.QuestionID,
                  Chartt: "pie",
                  Answered: element.Answered,
                  skipped: element.Skipped
                }
              ]
            }
          this.options.push(option);
        }
        index++;
      })

    }



  }
}
