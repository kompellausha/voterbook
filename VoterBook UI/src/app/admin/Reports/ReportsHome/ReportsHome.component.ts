import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { VoteBookService } from "app/vote-book.service";
import { ElementRef, Renderer2, ViewChild, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-ChartReportDetail',
    templateUrl: './ReportsHome.component.html',
    styleUrls: ['./ReportsHome.component.css']
})
export class ReportsHomeComponent implements OnInit {
    listSurveys: any[] = [];
    constructor(private router: Router, private route: ActivatedRoute,private voterbookservice: VoteBookService) {
    }

    ngOnInit(): void {
        this.voterbookservice.GetSurveys().then(survey => {
            console.log(survey)
            this.listSurveys = survey
        })
    }

    GetDetailsSurveyReport(SurveyMasterId: number) {      
       this.router.navigate(['./',SurveyMasterId,'Detail'],{relativeTo:this.route});
    }


}
