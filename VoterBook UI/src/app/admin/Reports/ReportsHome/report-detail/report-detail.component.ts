import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";
import { Router } from "@angular/router";
import { VoteBookService } from "app/vote-book.service";
@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css']
})
export class ReportDetailComponent implements OnInit {
  id: number;
  TotalResponses: number = 0;
  surveydetails: any = { SurveyName: '', IsActive: '' };

  responsedatescount: any[] = [];
  responsedates: any[] = [];

  responseAreacount: any[] = [];
  responseArea: any[] = [];


  responseAgecount:any[]=[];
  responseAge:any[]=[];


  responseGenderCount:any[]=[];
  responseGender:any[]=[];
  // Opt:any={};
  // optionss:any={};

  Optdate: any = {};
  Optarea: any = {};
  Optage: any = {};
  Optgender: any = {};

  constructor(private router: Router, private route: ActivatedRoute, private voterbookservice: VoteBookService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      this.voterbookservice.GetSurveySummary(this.id).then(survey => {
        this.TotalResponses = survey.TotalResponses;
        this.surveydetails = survey.SurveyDetails[0];
        for (let i = 0; i < survey.query.length; i++) {
          let str = survey.query[i].SurveyDates.substring(0, survey.query[i].SurveyDates.length - 9);
          this.responsedatescount.push(survey.query[i].Count);
          this.responsedates.push(str);
        }
        this.Optdate = this.columnchart(this.responsedates, this.responsedatescount, "Date wise Report");
      })

      this.voterbookservice.GetSurveySummaryAge(this.id).then(age => {
       
       let Data = [];
        for (let i = 0; i < age.length; i++) {
          let dat = {name:'',y:0}
          this.responseAreacount.push(age[i].Count);
          this.responseArea.push(age[i].Age);
          dat.name = age[i].Age;
          dat.y=age[i].Count;
          Data.push(dat);
        }       
       this.Optage=  this.pieChart(this.responseArea,Data);
      
      })

      this.voterbookservice.GetSurveySummaryArea(this.id).then(area => {

        for (let i = 0; i < area.length; i++) {
          this.responseAreacount.push(area[i].Count);
          this.responseArea.push(area[i].Area);
        }
        this.Optarea = this.columnchart(this.responseArea, this.responseAreacount, "Area wise Report");

      })

      this.voterbookservice.GetSurveySummaryGender(this.id).then(gender => {      
        let Data = [];
        for (let i = 0; i < gender.length; i++) {
          let dat = {name:'',y:0}
          this.responseAreacount.push(gender[i].Count);
          this.responseArea.push(gender[i].Gender);
          dat.name = gender[i].Gender;
          dat.y=gender[i].Count;
          Data.push(dat);
        }       
       this.Optgender=  this.pieChart(this.responseArea,Data);
      })

    });

  }

  GenerateChart(data) {

    // let Data: any[] = [];
    //  let optionsDesc = this.responsedates;
    //         var Datafinal = {};
    //         optionsDesc.forEach((key, i) => {
    //           Datafinal[key] = this.responsecount[i]
    //         });

    //        for (const key of Object.keys(Datafinal)) {
    //             let dataform = { name: '', y: 0 };
    //             dataform.name = key;
    //             dataform.y = Datafinal[key];
    //             Data.push(dataform);
    //           }

    // let option =
    //       {
    //         chart: {
    //           type: "pie"
    //         },
    //         plotOptions: {
    //           pie: {
    //             allowPointSelect: true,
    //             cursor: 'pointer',
    //             dataLabels: {
    //               enabled: true,
    //               format: '<label><b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)</label>',
    //               // <b>{point.name}</b>:
    //                                //     distance:-20,
    //             },
    //             showInLegend: true
    //           }
    //         },
    //         title: { text: '' },
    //         tooltip: {
    //           //  pointFormat: '{series.name}: <b>{point.y}</b>'
    //           pointFormat: '{series.name}: <b><b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)</b>'
    //         },
    //         xAxis: {
    //           categories: this.responsedates
    //         },
    //         series: [{
    //           name: 'selected:',
    //           colorByPoint: true,
    //           data: data
    //         }],

    //       }
    //       this.Opt = option;
    //       this.optionss = this.Opt;    


    //  let option =
    //       {
    //         chart: {
    //           type: "column"
    //         },
    //         plotOptions: {
    //           column: {
    //             pointPadding: 0.2,
    //             borderWidth: 0
    //           }
    //         },
    //         title: { text: '' },
    //         tooltip: {
    //             pointFormat: '{series.name}: <b><b>{point.y}</b></b>'

    //         },
    //         xAxis: {
    //           categories: this.responsedates              
    //         },
    //         yAxis: {
    //           min: 0,
    //           title: {
    //             text: '',
    //           },

    //         },
    //         series: [{
    //           showInLegend: false,
    //           name: 'Participated',
    //           data: this.responsecount
    //         }],          
    //       }
    //       this.Optdate = option;

  }

  gotoAnalyze() {
    this.router.navigate(['./', 'Analyze'], { relativeTo: this.route });
  }

  pieChart(xAxisData, data) {

    let option =
      {
        chart: {
          type: "pie"
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              format: '<label><b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)</label>',
              // <b>{point.name}</b>:
              //     distance:-20,
            },
            showInLegend: true
          }
        },
        title: { text: '' },
        tooltip: {
          //  pointFormat: '{series.name}: <b>{point.y}</b>'
          pointFormat: '{series.name}: <b><b>{point.y}</b> (<b>{point.percentage:.1f}%</b>)</b>'
        },
        // xAxis: {
        //   categories: xAxisData
        // },
        series: [{
          name: 'selected:',
          colorByPoint: true,
          data: data
        }],

      }
    return option;

  }

  columnchart(XaxisData, YaxisData, title) {
    let option =
      {
        chart: {
          type: "column"
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
        },
        title: { text: title },
        tooltip: {
          pointFormat: '{series.name}: <b><b>{point.y}</b></b>'

        },
        xAxis: {
          categories: XaxisData
        },
        yAxis: {
          min: 0,
          title: {
            text: title,
          },

        },
        series: [{
          showInLegend: false,
          name: 'Participated',
          data: YaxisData
        }],
      };
    return option;

  }

}