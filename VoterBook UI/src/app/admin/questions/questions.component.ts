import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Form } from "@angular/forms";
import { VoteBookService } from "app/vote-book.service";
// import { Survey } from "app/admin/survey/survey.model";
import { ActivatedRoute, Params, Router } from "@angular/router";
declare var jQuery: any;
@Component({
  selector: 'app-test-questions',
  templateUrl: 'questions.component.html',
  styleUrls: ['questions.component.css']
})
export class QuestionsComponent implements OnInit {
  id: any;
  editMode = false;
  QuestionsForm: FormGroup;
  form: FormGroup;
  surveys: Survey[] = [];
  selectedSurveyID: number;
  selectedSurvey = { surveyname: '', surveyid: 0 };
  questions: Question[] = [];
  isFirstQuestion = true;
  QuestionCount: number = 0;
  QuestionsCountArray: number[] = [];
  ShowDefquesorder: boolean = false;
  EmptyFirstQuestion: boolean = true;

  constructor(private ref: ChangeDetectorRef, private _elRef: ElementRef, private fb: FormBuilder, private surveyService: VoteBookService,
    private questionService: VoteBookService,private router:Router,
    private route: ActivatedRoute) {
  }

  intialise() {
    this.QuestionsForm = this.fb.group({
      Questions: this.fb.array([this.fb.group({
        QuestionId: 0,
        QuestionText: '',
        QuestionType: 'Single',
        SurveyID: '',
        LogicType: 1,
        OrderNumber: 0,
        NextQuestionId: '',
        Options: this.fb.array([this.fb.group({ OptionValue: '', NextQuestionId: '' })])
      })])
    });
    this.QuestionCount = 1;
  }


  ngOnInit() {
    this.intialise();
    this.JquerySorting();
    this.route.params.subscribe((params: Params) => {
      this.id = +params['id'];
      console.log(this.id);
      if (this.id != 0) {
        this.editMode = true;
        this.surveyService.GetSurveys().then(
          res => {
            this.surveys = res;
            console.log(res);
            let surveylist = res
            surveylist.forEach(element => {
              if (element.SurveyMasterId === this.id) {
                this.selectedSurvey.surveyname = element.SurveyName;
                this.selectedSurvey.surveyid = this.id;
                console.log(this.selectedSurvey);
              }
            });
          });
        this.questionService.getQuestions(this.id).then(
          res => {
            this.questions = res;
            console.log(res);
            let questionlist: Array<Question> = res;
            questionlist.forEach(element => {
              if (element.SurveyID === this.id) {
                if (questionlist.length > 0) {
                  this.QuestionsForm = this.fb.group({
                    Questions: this.GetQuestionGroupArray(questionlist)
                  });
                  this.QuestionCount = questionlist.length;
                  questionlist.forEach(element => {
                    let questions = this.QuestionsForm.get("Questions") as FormArray;
                    //questions.push()            
                  });
                } else {
                  this.EmptyFirstQuestion = true;
                  this.QuestionsForm = this.fb.group({
                    Questions: this.fb.array([this.fb.group({
                      QuestionId: 0,
                      QuestionText: '',
                      QuestionType: 'Single',
                      SurveyID: '',
                      LogicType: 1,
                      OrderNumber: 0,
                      NextQuestionId: '',
                      Options: this.fb.array([this.fb.group({ OptionValue: null, NextQuestionId: '' })])
                    })])
                  });
                }

              }
            })

          }
        )
      }
    });
  }
  AddNewQuestion() {
    console.log(this.QuestionsForm.value.Questions)
    this.EmptyFirstQuestion = false;
    let questions = this.QuestionsForm.get("Questions") as FormArray;
    let QuestionCount = questions.controls.length;
    questions.push(this.fb.group({
      QuestionId: 0,
      QuestionText: '',
      QuestionType: 'Single',
      SurveyID: '',
      LogicType: 1,
      OrderNumber: 0,
      NextQuestionId: '',
      Options: this.fb.array([this.fb.group({ OptionValue: '', NextQuestionId: '' })])
    }));
    this.QuestionsCountArray = [];
    this.QuestionCount = this.QuestionCount + 1;
    for (let i = 1; i <= this.QuestionCount; i++) {
      this.QuestionsCountArray[i - 1] = i;
    }
    this.AssignValue();

  }

  GetQuestionGroupArray(elemlist: Array<Question>) {
    let groupList: Array<FormGroup> = [];
    elemlist.forEach(element => {
      this.QuestionsForm = this.fb.group({
        QuestionText: element.QuestionText,
        QuestionType: element.QuestionType,
        SurveyID: element.SurveyID,
        QuestionId:element.QuestionID,
        LogicType: element.LogicType,
        OrderNumber: element.OrderNumber,
        NextQuestionId: element.NextQuestionId,
        Options: this.GetOptionsArray(element.Options)//this.fb.array([this.fb.group({ OptionValue: null })])
      });
      groupList.push(this.QuestionsForm);
      this.ShowDefquesorder = true;
      this.EmptyFirstQuestion = false;
      this.QuestionsCountArray = [];
      this.QuestionCount = elemlist.length;
      for (let i = 1; i <= this.QuestionCount; i++) {
        this.QuestionsCountArray[i - 1] = i;
      }

    })
    return this.fb.array(groupList);
  }

  GetOptionsArray(options: Array<Option>) {
    let optionlist: Array<FormGroup> = [];
    options.forEach(element => {
      let fg = this.fb.group({
        OptionValue: element.OptionValue,
        OptionID: element.OptionID,
        NextQuestionId: element.NextQuestionId
      });
      optionlist.push(fg);
    });
    return this.fb.array(optionlist);
  }

  AddOption(i) {
    let questions = this.QuestionsForm.get("Questions") as FormArray;
    let options = questions.at(i).get("Options") as FormArray;
    options.push(this.fb.group({ OptionValue: '', NextQuestionId: '' }));
  }
  onSave() {
    this.AssignValue();


    // let Questions = this.QuestionsForm.value;
    // let QuestionsList: Question[] = [];

    // Questions.Questions.forEach(element => {
    //   let Options: Option[] = [];
    //   console.log(element.Options);
    //   element.Options.forEach(x => {
    //     Options.push(new Option(0, "", x.OptionValue, ""));
    //   })
    //   QuestionsList.push(new Question(0, this.selectedSurveyID, element.QuestionText, element.QuestionType, Options, element.LogicType, element.OrderNumber));
    // });
    // console.log(QuestionsList);
    // this.questionService.saveQuestions(QuestionsList);
    let survey: Survey;
    survey = new Survey(this.id, "Sample", this.QuestionsForm.value.Questions);
    this.questionService.saveQuestions(survey);
    this.router.navigate(['/admin/survey']);
  }
  onChange(value) {
    this.selectedSurveyID = value;
  }


  DeleteQuestion(index) {
    const control = <FormArray>this.QuestionsForm.controls['Questions'];
    control.removeAt(index);

    this.QuestionsCountArray = [];
    this.QuestionCount = this.QuestionCount - 1;
    for (let i = 1; i <= this.QuestionCount; i++) {
      this.QuestionsCountArray[i - 1] = i;
    }
  }

  DeleteOption(Questionindex, OptionIndex) {
    const Question = this.QuestionsForm.controls['Questions']["controls"][Questionindex]["controls"]["Options"]["controls"] as Array<FormGroup>;
    Question.splice(OptionIndex, 1);
  }

  getnextdefaultQuestion(i) {
    const control = <FormArray>this.QuestionsForm.controls['Questions'];
    if (control.controls.length == i + 1) {
      return 0;
    }
    return i + 2;
  }

  ConfigureOption(s, value) {
    //  this.AddNewQuestion();
    // const control = <FormArray>this.QuestionsForm.controls['Questions'];
    // control.removeAt(control.length-1);

  }


  gettrue(i) {
    // const control = <FormArray>this.QuestionsForm.controls['Questions'];
    //   if(control.value[i].LogicType == 3)
    //   {      
    //     return true;
    //   }
    return true;
  }

  getskip(i) {
    // const control = <FormArray>this.QuestionsForm.controls['Questions'];
    //   if(control.value[i].LogicType == 2)
    //   {
    //     return true;
    //   }
    return true;

  }


  AssignValue() {
    const Ques = this.QuestionsForm.get("Questions") as FormArray;
    let order = 1;
    for (let x = 0; x < Ques.controls.length; x++) {
      if (x != Ques.controls.length - 1) {
        if (Ques.controls[x]["value"].LogicType == 1) {
          const control = new FormControl(order + 1);
          Ques.controls[x]["controls"].NextQuestionId = control;
        }
        order++;
      }
      else {
        this.QuestionsForm.value["Questions"][x]["OrderNumber"] = 0;
      }
    }
  }

  JquerySorting() {
    // jQuery(this._elRef.nativeElement).find('#grid').sortable({
    //   tolerance: 'pointer',
    //   revert: 'invalid',
    //   placeholder: 'span2 well placeholder tile',
    //   forceHelperSize: true,
    //   stop: function (event, ui) {
    //   }
    // });

    jQuery('select option')
      .filter(function () {
        return !this.value || jQuery.trim(this.value).length == 0;
      })
      .remove();
  }




}

export class Survey {
  SurveyID: number;
  SurveyName: string;
  Questions: Question[]
  constructor(SurveyID: number, SurveyName: string, Question: Question[]) {
    this.SurveyID = SurveyID;
    this.SurveyName = SurveyName;
    this.Questions = Question;

  }
}


export class Question {
  QuestionID: number;
  SurveyID: number;
  QuestionText: string;
  QuestionType: string;
  LogicType: number;
  Options: Option[];
  OrderNumber: number;
  NextQuestionId: string;
  constructor(QuestionID: number, SurveyID: number, QuestionText: string, QuestionType: string, Option: Option[], LogicType: number, OrderNumber: number, NextQuestionId: string) {
    this.QuestionID = QuestionID;
    this.SurveyID = SurveyID;
    this.QuestionText = QuestionText;
    this.QuestionType = QuestionType;
    this.Options = Option;
    this.LogicType = LogicType;
    this.OrderNumber = OrderNumber;
    this.NextQuestionId = NextQuestionId;
  }
}
export class Option {
  OptionID: number;
  OptionKey: string;
  OptionValue: string;
  OptionSelected: string;
  NextQuestionId: string;

  constructor(OptionID: number, OptionKey: string, OptionValue: string, OptionSelected: string, NextQuestionId: string) {
    this.OptionID = OptionID;
    this.OptionKey = OptionKey;
    this.OptionValue = OptionValue;
    this.OptionSelected = OptionSelected;
    this.NextQuestionId = NextQuestionId
  }
}
