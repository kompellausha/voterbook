import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionsComponent } from './questions.component';

describe('QuestionsComponent', () => {
  let component: QuestionsComponent;
  let fixture: ComponentFixture<QuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});





import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, FormArray, Form } from "@angular/forms";
import { VoteBookService } from "app/vote-book.service";
import { Survey } from "app/admin/survey/survey.model";
declare var jQuery: any;
@Component({
  selector: 'app-test-questions',
  templateUrl: 'questions.component.html',
  styleUrls: ['questions.component.css']
})
export class QuestionsComponent implements OnInit {

  QuestionsForm: FormGroup;
  form: FormGroup;
  surveys: Survey[] = [];
  selectedSurveyID: number;
  isFirstQuestion = true;
  QuestionCount: number = 0;
  QuestionsCountArray: number[] = [];
  constructor(private _elRef: ElementRef, private fb: FormBuilder, private surveyService: VoteBookService,
    private questionService: VoteBookService) {
    // this.form=new FormGroup({
    //   surveyname:new FormControl('')
    // })
    this.QuestionsForm = this.fb.group({
      Questions: this.fb.array([this.fb.group({
        QuestionId: 0,
        QuestionText: '',
        QuestionType: '',
        SurveyID: '',
        LogicType: '',
        NextQuestionID: 0,
        Options: this.fb.array([this.fb.group({ OptionValue: '' })])
      })])
    });
    this.QuestionCount = 1;
    console.log(this.QuestionsForm);
    console.log(this._elRef);
    console.log(jQuery);
  }
  AddNewQuestion() {
    this.isFirstQuestion = false;
    let questions = this.QuestionsForm.get("Questions") as FormArray;
    console.log(questions);
    let QuestionCount = questions.controls.length;
    // let questionid = this.QuestionsForm.controls.Questions["_value"];     
    questions.push(this.fb.group({
      QuestionId: 0,
      QuestionText: '',
      QuestionType: '',
      SurveyID: '',
      LogicType: '',
      NextQuestionID: 0,
      Options: this.fb.array([this.fb.group({ OptionValue: '' })])
    }));
    this.QuestionsCountArray = [];
    this.QuestionCount = this.QuestionCount + 1;
    for (let i = 1; i <= this.QuestionCount; i++) {
      this.QuestionsCountArray[i - 1] = i;
    }
    console.log(this.QuestionsForm);
  }
  ngOnInit() {
    this.surveyService.GetSurveys().then(
      res => {
        this.surveys = res;
      });

    jQuery(this._elRef.nativeElement).find('#grid').sortable({
      tolerance: 'pointer',
      revert: 'invalid',
      placeholder: 'span2 well placeholder tile',
      forceHelperSize: true,
      stop: function (event, ui) {
        console.log(event);
        console.log(ui);
      }
    });
  }
  AddOption(i) {
    let questions = this.QuestionsForm.get("Questions") as FormArray;
    let options = questions.at(i).get("Options") as FormArray;
    options.push(this.fb.group({ OptionValue: '' }));
  }
  onSave() {
    console.log(this.QuestionsForm.value);
    let Questions = this.QuestionsForm.value;
    let QuestionsList: Question[] = [];

    Questions.Questions.forEach(element => {
      let Options: Option[] = [];
      console.log(element.Options);
      element.Options.forEach(x => {
        Options.push(new Option(0, "", x.OptionValue, ""));
      })
      QuestionsList.push(new Question(0, this.selectedSurveyID, element.QuestionText, element.QuestionType, Options));
    });
    console.log(QuestionsList);
    //save Method()
    this.questionService.saveQuestions(QuestionsList);
  }
  onChange(value) {
    this.selectedSurveyID = value;
  }

  // addQuestion(loopno) {
  //   console.log("loop",loopno);
  //   console.log(this.QuestionsCountArray);
  //   for (let c = 0; c < this.QuestionsCountArray.length; c++) {
  //     if (loopno + 1 != this.QuestionsCountArray[c]) {
  //       return this.QuestionsCountArray[c];
  //     }
  //   }

  // }
}
export class Question {
  QuestionID: number;
  SurveyID: number;
  QuestionText: string;
  QuestionType: string;
  Options: Option[];
  constructor(QuestionID: number, SurveyID: number, QuestionText: string, QuestionType: string, Option: Option[]) {
    this.QuestionID = QuestionID;
    this.SurveyID = SurveyID;
    this.QuestionText = QuestionText;
    this.QuestionType = QuestionType;
    this.Options = Option;
  }
}
export class Option {
  OptionID: number;
  OptionKey: string;
  OptionValue: string;
  OptionSelected: string;

  constructor(OptionID: number, OptionKey: string, OptionValue: string, OptionSelected: string) {
    this.OptionID = OptionID;
    this.OptionKey = OptionKey;
    this.OptionValue = OptionValue;
    this.OptionSelected = OptionSelected;
  }
}
