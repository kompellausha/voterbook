export class Questions{
question:Question[];
option:Option[];
}
export class Question{
    QuestionId:number;
    QuestionText='';
    QuestionType='';
    SurveyID='';
    LogicType:number;
    NextQuestionID:number;
}
export class Option{
    OptionId:number;
    OptionValue='';
}
export const types=['single','multiple'];