import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-home',
  template: `
    <div class="text-center h1"> Welcome to VoterBook</div>
  `,
  styles: []
})
export class AdminHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
