import { Component, OnInit } from '@angular/core';
import { VoteBookService } from "app/vote-book.service";
import { Survey } from "app/admin/survey/survey.model";

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {
  surveys:Survey[]=[];

  constructor(private surveyService:VoteBookService) { }

  ngOnInit() {
    this.surveyService.GetSurveys().then(
      res=>{
        this.surveys=res;
        console.log(res);
      }
    );
  }

}
