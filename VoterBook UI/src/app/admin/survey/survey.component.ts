import { Component, OnInit, Input } from '@angular/core';
import { VoteBookService } from "app/vote-book.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { LocalDataService } from "app/LocalDataService";
import { Router, ActivatedRoute } from "@angular/router";
import { Survey } from "app/admin/survey/survey.model";

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
  @Input() Userdetails: any = {};
  surveys: Array<Survey> = [];
  id: number;
  checkbox = false;
  surveyForm: FormGroup;

  constructor(private localdata: LocalDataService, private surveyService: VoteBookService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.surveyService.GetSurveys().then(
      res => {
        this.surveys = res;
        console.log(res);
      });
    this.intilizeform();
  }
  intilizeform() {
    let formControls: {} = {};
    formControls["SurveyName"] = new FormControl();
    this.surveyForm = new FormGroup(formControls);

  }
  saveSurvey() {
    let survey = { SurveyMasterId: 0, SurveyName: '', SurveyAnswerTable: '', IsActive: 0, UserId: 3 };
    survey.SurveyName = this.surveyForm.value.SurveyName;
    console.log("test", survey);
    this.surveyService.saveSurvey(survey).then(resp=>{
      alert("Create new Survey : "+survey.SurveyName);
      this.surveyService.GetSurveys().then(resp=>{
        this.surveys=resp;
        this.surveyForm.reset({});
      });
    });
  }
  onDisplay() {
    this.router.navigate(['./survey-list'], { relativeTo: this.route });
  }
  onEdit(survey) {
    let id = survey.SurveyMasterId;
    console.log(id);
    this.router.navigate(['/admin/questions', id]);
  }
  UpdateSurveyState($event,i){
    console.log("updating"+i);
    if($event.target.checked){
      this.surveyService.MakeSurveyActive(i).then(response=>{
        this.surveyService.GetSurveys().then(res=>{
          this.surveys=res;
        })
      })
    }
    console.log($event);
    console.log(i);
  }
}

