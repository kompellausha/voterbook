
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "app/login/login.component";
import { HomeComponent } from "app/home/home.component";
import { AdminComponent } from "app/admin/admin.component";
import { UsersComponent } from "app/admin/users/users.component";
import { UsereditComponent } from "app/admin/users/user-edit/user-edit.component";
import { UserDetailComponent } from "app/admin/users/user-detail/user-detail.component";
import { SurveyComponent } from "app/admin/survey/survey.component";
import { SurveyListComponent } from "app/admin/survey/survey-list/survey-list.component";
import { QuestionsComponent } from "app/admin/questions/questions.component";
import { ChartReportsComponent } from "app/admin/Reports/ChartReports/ChartReports.component";
import { AuthGuard } from "app/auth-guard.service";
import { ReportsHomeComponent } from "app/admin/Reports/ReportsHome/ReportsHome.component";
import { ReportDetailComponent } from "app/admin/Reports/ReportsHome/report-detail/report-detail.component";
import { AdminHomeComponent } from "app/admin/admin-home.component";



// const appRoutes: Routes = [
//     { path: '', redirectTo: '/login', pathMatch: 'full' },
//     { path: 'login', component: LoginComponent },
//     {
//         // path: 'admin', component: AdminComponent, canActivate: [AuthGuard], children: [
//         //     { path: 'user', component: UsersComponent },
//         //     { path: 'user/:id', component: UserDetailComponent },
//         //     { path: 'user/:id/edit', component: UsereditComponent },
//         //     { path: 'user/:id/new', component: UsereditComponent },
//         //     { path: 'survey', component: SurveyComponent,},
//         //     { path: 'questions', component: QuestionsComponent },
//         //     { path: 'Reports', component: ChartReportsComponent }
//         // ]
//              path: 'admin', component: AdminComponent,children: [
//             { path: 'user', component: UsersComponent },
//             { path: 'user/:id', component: UserDetailComponent },
//             { path: 'user/:id/edit', component: UsereditComponent },
//             { path: 'user/:id/new', component: UsereditComponent },
//             { path: 'survey', component: SurveyComponent,},
//             { path: 'questions', component: QuestionsComponent },
//             { path: 'Reports', component: ChartReportsComponent }
//         ]
//     },
//     { path: 'home', component: HomeComponent },

// ]


const appRoutes:Routes=[
{path:'',redirectTo:'/login',pathMatch:'full'},
{path:'login',component:LoginComponent},
{path:'admin',component:AdminComponent,children:[
{path:'',component:AdminHomeComponent,pathMatch:'full'},    
{path:'user',component:UsersComponent},
{path:'user/:id',component:UserDetailComponent},
{path:'user/:id/edit',component:UsereditComponent},
{path:'user/:id/new',component:UsereditComponent},
{path: 'Reports', component: ReportsHomeComponent },
{path: 'Reports/:id/Detail', component: ReportDetailComponent },
{path: 'Reports/:id/Detail/Analyze', component: ChartReportsComponent },
{path: 'questions', component: QuestionsComponent },
{path:'survey',component:SurveyComponent,
// children:[
// {path:'survey-list',component:SurveyListComponent}
// ]
},
{path:'questions/:id',component:QuestionsComponent}
]},
{path:'home',component:HomeComponent}
]

@NgModule(
    {
        imports: [RouterModule.forRoot(appRoutes)],
        exports: [RouterModule]
    }
)

export class AppRoutingModule {

}