import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from "app/app-routing.module";
import { AuthService } from "app/auth.service";
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { UsersComponent } from "app/admin/users/users.component";
import { UsereditComponent } from "app/admin/users/user-edit/user-edit.component";

import { UserDetailComponent } from './admin/users/user-detail/user-detail.component';
import { SurveyComponent } from "app/admin/survey/survey.component";
import { SurveyListComponent } from "app/admin/survey/survey-list/survey-list.component";
import { VoteBookService } from "app/vote-book.service";
import { LocalDataService } from "app/LocalDataService";
import { QuestionsComponent } from "app/admin/questions/questions.component";
import { ChartModule } from 'angular2-highcharts';
import * as highcharts from 'highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { ChartReportsComponent } from "app/admin/Reports/ChartReports/ChartReports.component";
import { ChartControlComponent } from "app/admin/Reports/ChartReports/ChartControl.component";
import { AuthGuard } from "app/auth-guard.service";
import { DndModule } from 'ng2-dnd';
import { ReportsHomeComponent } from "app/admin/Reports/ReportsHome/ReportsHome.component";
import { ReportDetailComponent } from './admin/Reports/ReportsHome/report-detail/report-detail.component';
import { AdminHomeComponent } from './admin/admin-home.component';
declare var require: any;


export function highchartsFactory() {
      const hc = require('highcharts');
      const dd = require('highcharts/modules/drilldown');
      dd(hc);

      return hc;
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AdminComponent,
    UsersComponent,
    UsereditComponent,
    UserDetailComponent,
    SurveyComponent,
    SurveyListComponent,
    QuestionsComponent,   
    ChartReportsComponent,
    ChartControlComponent,
    ReportsHomeComponent,
    ReportDetailComponent,
    AdminHomeComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ChartModule, 
    DndModule.forRoot()
  ],
  providers: [{
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    },AuthService,VoteBookService,FormBuilder,LocalDataService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
